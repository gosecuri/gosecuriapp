public class Tools {

    private String id;
    private String tool;


    public Tools(String id, String tool) {

        this.id = id;
        this.tool = tool;
    }


    @Override
    public String toString() {
        return "Tools{" +
                "id='" + id + '\'' +
                ", tool='" + tool + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }
}
